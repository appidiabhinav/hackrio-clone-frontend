const purgecss=require("@fullhuman/postcss-purgecss");
const cssnano=require("cssnano");


module.exports={
	plugins:[
		require("tailwindcss"),
		require("autoprefixer"),
		process.env.NODE_ENV === 'production' && cssnano({
			preset:"default",}),
		process.env.NODE_ENV === 'production' && purgecss({
			content:[
				"./src/*.svelte",
				"./src/navbar_contents/*.svelte",
				],
			
		defaultExtractor:content=>content.match(/[A-Za-z0-9-_:/]+/g) || []
		}),
	],
	};
